import logo from './logo.svg'
import './App.css'

const Name = process.env.REACT_APP_MY_NAME

function App () {
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <h2>
          Hello {Name}!
        </h2>
      </header>
    </div>
  )
}

export default App
